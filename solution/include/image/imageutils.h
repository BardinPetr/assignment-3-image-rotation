#ifndef IMAGE_TRANSFORMER_IMAGEUTILS_H
#define IMAGE_TRANSFORMER_IMAGEUTILS_H

#include "image.h"

void image_destroy(struct image *img);

struct image *image_create(uint64_t width, uint64_t height);

int image_resize(struct image *img, uint64_t width, uint64_t height);

struct image *image_copy(const struct image *src);

struct pixel image_pixel_get(const struct image *img, uint64_t row, uint64_t col);

void image_pixel_set(struct image *img, uint64_t row, uint64_t col, struct pixel value);

#endif //IMAGE_TRANSFORMER_IMAGEUTILS_H
