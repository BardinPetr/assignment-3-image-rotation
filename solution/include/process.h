#ifndef IMAGE_TRANSFORMER_PROCESS_H
#define IMAGE_TRANSFORMER_PROCESS_H

enum process_result {
    PROCESS_OK = 0,
    PROCESS_ERROR_READ,
    PROCESS_ERROR_FORMAT_INVALID,
    PROCESS_ERROR_FORMAT_NOT_FOUND,
    PROCESS_ERROR_WRITE,
    PROCESS_ERROR_TRANSFORM,
    PROCESS_ERROR_TRANSFORM_INVALID
};

static const char *const process_result_messages[] = {
        [PROCESS_OK]="Processed successfully!",
        [PROCESS_ERROR_READ]="Failed to read input file",
        [PROCESS_ERROR_FORMAT_INVALID]="File is invalid",
        [PROCESS_ERROR_FORMAT_NOT_FOUND]="Input file format is not supported",
        [PROCESS_ERROR_WRITE]="Failed to write output file",
        [PROCESS_ERROR_TRANSFORM]="Failed to do transform",
        [PROCESS_ERROR_TRANSFORM_INVALID]="Invalid transform requested"
};

enum process_result process(const char *filename_input, const char *filename_output, const char *operation);

#endif //IMAGE_TRANSFORMER_PROCESS_H
