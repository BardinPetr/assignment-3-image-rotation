#ifndef IMAGE_TRANSFORMER_APP_TRANSFORMS_H
#define IMAGE_TRANSFORMER_APP_TRANSFORMS_H

#include "transform/transform.h"
#include <stddef.h>

const struct transform_def *transform_by_request(const char *request_string);

extern const struct transform_def *const TRANSFORMS[];

extern const size_t TRANSFORMS_COUNT;

#endif //IMAGE_TRANSFORMER_APP_TRANSFORMS_H
