#ifndef IMAGE_TRANSFORMER_TRANSFORM_ROTATE_H
#define IMAGE_TRANSFORMER_TRANSFORM_ROTATE_H

#include "image/image.h"
#include "transform/transform.h"

// mapping of angle divided by 90 degrees to rotate types
enum ROTATE_DIR {
    ROTATE_NONE = 0,
    ROTATE_90_CLOCKWISE = 90 / 90,
    ROTATE_HORIZONTAL_REFLECT = 180 / 90,
    ROTATE_90_COUNTERCLOCKWISE = 270 / 90,
};

struct image *rotate(const struct image *src, const char *params);

int is_rotate(const char *command);

extern const struct transform_def ROTATE_TRANSFORM;

#endif //IMAGE_TRANSFORMER_TRANSFORM_ROTATE_H
