#ifndef IMAGE_TRANSFORMER_TRANSFORM_H
#define IMAGE_TRANSFORMER_TRANSFORM_H

#include "image/image.h"

typedef struct image *(*transform)(const struct image *, const char *);

typedef int (*check_transform)(const char *);

struct transform_def {
    const char *info_text;
    const char *name;
    check_transform check_request;
    transform apply;
};


#endif //IMAGE_TRANSFORMER_TRANSFORM_H
