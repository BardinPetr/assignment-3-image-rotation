#ifndef IMAGE_TRANSFORMER_FORMAT_BMP_H
#define IMAGE_TRANSFORMER_FORMAT_BMP_H

#include "format/format.h"

enum read_status from_bmp(FILE *, struct image *);

enum write_status to_bmp(FILE *, struct image const *);

extern const struct file_format_io BMP_FORMAT_IO;

#endif //IMAGE_TRANSFORMER_FORMAT_BMP_H

