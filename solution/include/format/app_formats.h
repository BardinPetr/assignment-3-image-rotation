#ifndef IMAGE_TRANSFORMER_APP_FORMATS_H
#define IMAGE_TRANSFORMER_APP_FORMATS_H

#include "format/format.h"
#include <stddef.h>
#include <stdio.h>

const struct file_format_io *file_format_by_file(FILE *file);

extern const struct file_format_io * const FILE_FORMATS[];

extern const size_t FILE_FORMATS_COUNT;

#endif //IMAGE_TRANSFORMER_APP_FORMATS_H
