#ifndef IMAGE_TRANSFORMER_FORMAT_H
#define IMAGE_TRANSFORMER_FORMAT_H

#include "image/image.h"
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_ERR_NO_FILE,
    READ_ERR_FAILED,
    READ_INVALID_HEADER,
    READ_INVALID_CONTENT,
    READ_INVALID_FORMAT,
    READ_INVALID_COLORS,
    READ_INVALID_SIZE
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERR_NO_FILE,
    WRITE_INVALID_SOURCE,
    WRITE_FORMAT_NOT_APPLICABLE,
    WRITE_ERR_FAILED,
};

static const char *const read_status_messages[] = {
        [READ_OK]="Read successful",
        [READ_ERR_NO_FILE]="File not opened",
        [READ_ERR_FAILED]="Failed to read from supplied file",
        [READ_INVALID_HEADER]="Image header corrupted or invalid",
        [READ_INVALID_CONTENT]="Image data is not in agreement with header",
        [READ_INVALID_FORMAT]="Image format unsupported",
        [READ_INVALID_COLORS]="Image color resolution not supported",
        [READ_INVALID_SIZE]="Failed to allocate memory for image"
};

static const char *const write_status_messages[] = {
        [WRITE_OK]="Write successful",
        [WRITE_ERR_NO_FILE]="File not opened",
        [WRITE_INVALID_SOURCE]="Source image is invalid",
        [WRITE_FORMAT_NOT_APPLICABLE]="Internal image format could not be converted by implemented BMP file handlers",
        [WRITE_ERR_FAILED]="Failed to write to specified file"
};

/**
 * File format deserialization function pointer
 * Read image from binary file to supplied image object pointer
 * Function represents specific format implementation
 * @param file opened input file
 * @param img  target image pointer (image->data will be reallocated according to new size)
 * @return status of operation, see format/format.h
 */
typedef enum read_status (*from_file)(FILE *, struct image *);

/**
 * File format serialization function pointer
 * Write image from object to specified binary file
 * Function represents specific format implementation
 * @param file opened output file
 * @param img  image to be written
 * @return status of operation, see format/format.h
 */
typedef enum write_status (*to_file)(FILE *, struct image const *);

/**
 * Defines serializer and deserializer for image format
 */
struct file_format_io {
    const char *name;       // UI-only name
    const char *signature;  // for auto-detection based on file signature

    from_file load;
    to_file store;
};

#endif //IMAGE_TRANSFORMER_FORMAT_H
