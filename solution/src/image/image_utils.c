#include "image/imageutils.h"
#include <malloc.h>
#include <string.h>

/**
 * For given image object change dimensions.
 * Deallocate older data pointer and allocate again pixel memory set with 0
 * @param img       initialized image pointer
 * @param width
 * @param height
 */
int image_resize(struct image *img, uint64_t width, uint64_t height) {
    if (img == NULL)
        return 0;
    if (img->data != NULL)
        free(img->data);

    img->data = calloc(width * height, sizeof(struct pixel));
    if (img->data == NULL)
        return 0;

    img->height = height;
    img->width = width;
    return 1;
}

/**
 * Create image with given dimension and allocate memory for data
 * @param width
 * @param height
 * @return pointer to allocated struct image with image->data allocated too
 */
struct image *image_create(uint64_t width, uint64_t height) {
    struct image *res = malloc(sizeof(struct image));
    if (res == NULL)
        return NULL;

    res->data = NULL;
    if (!image_resize(res, width, height)) {
        free(res);
        return NULL;
    }
    return res;
}

/**
 * Deallocate image struct and data pointer
 * @param img target image object
 */
void image_destroy(struct image *img) {
    if (img == NULL)
        return;
    if (img->data != NULL)
        free(img->data);
    free(img);
}

/**
 * Copy image data and parameters to new image object
 * @param src input image object
 * @return pointer to new image
 */
struct image *image_copy(const struct image *src) {
    if (src == NULL)
        return NULL;

    struct image *dst = image_create(src->width, src->height);
    if (dst == NULL)
        return NULL;

    memcpy(dst->data, src->data, src->width * src->height * sizeof(struct pixel)); // NOLINT(clang-analyzer-security.insecureAPI.DeprecatedOrUnsafeBufferHandling)
    return dst;
}

/**
 * Get linear index from row,col for given image
 * @param img input image object
 * @param row row index from 0
 * @param col column index from 0
 * @return position in image data array of pixel
 */
inline static uint64_t get_image_pos(const struct image *img, uint64_t row, uint64_t col) {
    return (row * img->width + col) % (img->width * img->height);
}

/**
 * Get pixel by row/column
 */
struct pixel image_pixel_get(const struct image *img, uint64_t row, uint64_t col) {
    return *(img->data + get_image_pos(img, row, col));
}

/**
 * Set pixel by row/column
 */
void image_pixel_set(struct image *img, uint64_t row, uint64_t col, struct pixel value) {
    *(img->data + get_image_pos(img, row, col)) = value;
}
