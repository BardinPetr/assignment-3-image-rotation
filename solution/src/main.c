#include "process.h"
#include "transform/app_transforms.h"
#include "utils.h"
#include <stdio.h>

void print_help(void) {
    printf("\nHelp\n"
           "Image transformer\n"
           "Usage:\n"
           "./image-transformer <source-image> <transformed-image> <transform-args>\n\n"
           "Transforms available:\n"
    );

    for (size_t i = 0; i < TRANSFORMS_COUNT; i++)
        printf("- %s\n", TRANSFORMS[i]->info_text);
}

int main(int argc, char **argv) {
    if (argc < 4) {
        printf("Not all arguments supplied\n\n");
        print_help();
        return 1;
    }

    enum process_result res = process(argv[1], argv[2], argv[3]);
    if (res == PROCESS_OK) {
        printf("\n---\n%s", process_result_messages[res]);
    } else {
        printf_err("\n---\n%s", process_result_messages[res]);
        print_help();
    }

    return res;
}
