#include "format/app_formats.h"
#include "format/bmp/format_bmp.h"
#include <malloc.h>
#include <stdbool.h>
#include <string.h>

const struct file_format_io *const FILE_FORMATS[] = { // NOLINT(cppcoreguidelines-interfaces-global-init)
        &BMP_FORMAT_IO
};

const size_t FILE_FORMATS_COUNT = (sizeof(FILE_FORMATS) / sizeof(struct file_format_io *));


/**
 * Select file format loader by comparing file signature to format signatures
 * @param file opened file
 * @return file format definition (file_format_io), NULL if no format found or error occurred
 */
const struct file_format_io *file_format_by_file(FILE *file) {
    if (file == NULL)
        return NULL;

    for (size_t i = 0; i < FILE_FORMATS_COUNT; i++) {
        const char *sign = FILE_FORMATS[i]->signature;
        size_t sign_len = strlen(sign);
        char *sign_current = calloc(sign_len + 1, 1);

        rewind(file);
        size_t read_cnt = fread(sign_current, 1, sign_len, file);

        bool cmp = read_cnt == sign_len && strcmp(sign, sign_current) == 0;

        free(sign_current);

        if (cmp)
            return FILE_FORMATS[i];
    }
    return NULL;
}
