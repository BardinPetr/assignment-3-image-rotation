#define BYTE_PER_PIXEL 3
#define BIT_PER_PIXEL (8*BYTE_PER_PIXEL)
#define PADDING_CNT 4

#include "format/bmp/bmp.h"
#include "format/format.h"
#include "image/imageutils.h"
#include <memory.h>

/**
 * Calculate BMP row ending 0-padding to make byte length of row be divisible by PADDING_CNT
 * @param bytePerPixel  bytes per pixel struct
 * @param rowLength     row length in pixels
 * @return BMP row padding byte count
 */
__attribute__((always_inline)) inline static int get_row_padding_bytes(int bytePerPixel, uint64_t rowLength) {
    return (int) (PADDING_CNT - (rowLength * bytePerPixel % PADDING_CNT)) % PADDING_CNT;
}

/**
 * Get full image data size including
 * @param bytePerPixel  bytes per pixel struct
 * @param img           image object
 * @return byte count of image in BMP data format
 */
__attribute__((always_inline)) inline static uint64_t get_padded_image_bytes(int bytePerPixel, const struct image *img) {
    return img->height * (img->width * bytePerPixel + get_row_padding_bytes(bytePerPixel, img->width));
}

/**
 * Read image from binary file to supplied image object pointer
 * Function represents specific format implementation
 * @param file opened input file
 * @param img  target image pointer (image->data will be reallocated according to new size)
 * @return status of operation, see format/format.h
 */
enum read_status from_bmp(FILE *file, struct image *img) {
    if (file == NULL)
        return READ_ERR_NO_FILE;
    rewind(file);

    struct bmp_header header = {0};
    size_t cnt = fread(&header, 1, sizeof(struct bmp_header), file);
    if (cnt != sizeof(struct bmp_header))
        return READ_INVALID_HEADER;

    if (header.info_header.biPlanes != 1 ||
        header.info_header.biCompression != 0)
        return READ_INVALID_FORMAT;

    if (header.info_header.biBitCount != 8 * BYTE_PER_PIXEL)
        return READ_INVALID_COLORS;

    if (header.info_header.biHeight == 0 ||
        header.info_header.biWidth == 0)
        return READ_INVALID_CONTENT;

    // seek to data start
    if (fseek(file, header.file_header.bOffBits, SEEK_SET) != 0)
        return READ_ERR_FAILED;

    if (!image_resize(img, header.info_header.biWidth, header.info_header.biHeight))
        return READ_INVALID_SIZE;

    int padding = get_row_padding_bytes(BYTE_PER_PIXEL, img->width);

    for (uint64_t i = 0; i < img->height; i++) {
        if (fread(
                img->data + i * img->width,
                1,
                img->width * sizeof(struct pixel),
                file
        ) != img->width * sizeof(struct pixel))
            return READ_ERR_FAILED;

        // skip padding
        if (i != (img->height - 1)) {
            if (fseek(file, padding, SEEK_CUR) == -1)
                return READ_ERR_FAILED;
        }
    }

    return READ_OK;
}


/**
 * Write image from object to specified binary file
 * Function represents specific format implementation
 * @param file opened output file
 * @param img  image to be written
 * @return status of operation, see format/format.h
 */
enum write_status to_bmp(FILE *file, struct image const *img) {
    if (file == NULL)
        return WRITE_ERR_NO_FILE;
    if (img == NULL)
        return WRITE_INVALID_SOURCE;
    if (sizeof(struct pixel) != BYTE_PER_PIXEL)
        return WRITE_FORMAT_NOT_APPLICABLE;

    size_t image_size = get_padded_image_bytes(BYTE_PER_PIXEL, img);
    struct bmp_header header = {
            .file_header={
                    .bfType=0x4d42,
                    .bfileSize=sizeof(struct bmp_header) + image_size,
                    .bfReserved=0,
                    .bOffBits=sizeof(struct bmp_header)
            },
            .info_header={
                    .biSize=sizeof(struct bmp_info_header),
                    .biWidth=img->width,
                    .biHeight=img->height,
                    .biPlanes=1,
                    .biBitCount=BIT_PER_PIXEL,
                    .biCompression=0,
                    .biSizeImage=image_size,
                    .biXPelsPerMeter=0,
                    .biYPelsPerMeter=0,
                    .biClrUsed=0,
                    .biClrImportant=0
            }
    };

    int padding = (int) get_row_padding_bytes(BYTE_PER_PIXEL, img->width);
    if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1)
        return WRITE_ERR_FAILED;

    uint8_t paddings[PADDING_CNT] = {0};
    for (uint64_t r = 0; r < img->height; r++) {
        if (fwrite(img->data + r * img->width, sizeof(struct pixel), img->width, file) != img->width)
            return WRITE_ERR_FAILED;
        if (fwrite(&paddings, 1, padding, file) != padding)
            return WRITE_ERR_FAILED;
    }

    return WRITE_OK;
}

const struct file_format_io BMP_FORMAT_IO = {
        .name = "bmp",
        .signature = "BM",
        .load = from_bmp,
        .store = to_bmp
};
