#include "format/app_formats.h"
#include "image/imageutils.h"
#include "process.h"
#include "transform/app_transforms.h"
#include "utils.h"
#include <inttypes.h>
#include <stdio.h>

enum process_result process(const char *filename_input, const char *filename_output, const char *operation) {
    // Detect transform
    const struct transform_def *transform = transform_by_request(operation);
    if (transform == NULL)
        return PROCESS_ERROR_TRANSFORM_INVALID;
    printf("Selected transform: %s\n", transform->name);

    // Open file
    FILE *src_file = fopen(filename_input, "rb");
    if (src_file == NULL)
        return PROCESS_ERROR_READ;
    printf("File opened successfully\n");

    //Detect file format and read contents as image
    const struct file_format_io *format = file_format_by_file(src_file);
    if (format == NULL) {
        fclose(src_file);
        return PROCESS_ERROR_FORMAT_NOT_FOUND;
    }
    printf("Detected format: %s\n", format->name);

    struct image *src_image = image_create(0, 0);
    if (src_image == NULL)
        return PROCESS_ERROR_READ;

    enum read_status read_status = format->load(src_file, src_image);
    fclose(src_file);

    if (read_status != READ_OK || src_image == NULL) {
        printf_err(read_status_messages[read_status]);
        return PROCESS_ERROR_FORMAT_INVALID;
    }
    printf("Image (%"PRIu64"x%"PRIu64") loaded successfully\n", src_image->width, src_image->height);

    // Apply transform
    struct image *dst_image = transform->apply(src_image, operation);
    image_destroy(src_image);

    if (dst_image == NULL)
        return PROCESS_ERROR_TRANSFORM;

    printf("Image transformed to (%"PRIu64"x%"PRIu64")\n", dst_image->width, dst_image->height);
    // Write result
    FILE *dst_file = fopen(filename_output, "wb");
    if (dst_file == NULL) {
        image_destroy(dst_image);
        return PROCESS_ERROR_WRITE;
    }

    enum write_status write_status = format->store(dst_file, dst_image);
    image_destroy(dst_image);

    if (write_status != WRITE_OK) {
        printf_err(write_status_messages[write_status]);
        fclose(dst_file);
        return PROCESS_ERROR_WRITE;
    }
    printf("Result image written\n");

    fclose(dst_file);
    return PROCESS_OK;
}
