#include "utils.h"
#include <stdarg.h>
#include <stdio.h>

void printf_err(const char *fmt, ...) {
    va_list list;
    va_start(list, fmt);
    vfprintf(stderr, fmt, list); // NOLINT(clang-analyzer-valist.Uninitialized)
    va_end(list);
}
