#include "transform/app_transforms.h"
#include "transform/rotate/transform_rotate.h"

const struct transform_def *const TRANSFORMS[] = { // NOLINT(cppcoreguidelines-interfaces-global-init)
        &ROTATE_TRANSFORM
};

const size_t TRANSFORMS_COUNT = sizeof(TRANSFORMS) / sizeof(struct transform_def *);

/**
 * Search transform by checking supplied operation string over transform predicates
 * @param request_string console params input string
 * @return transform_def or NULL if not found
 */
const struct transform_def *transform_by_request(const char *request_string) {
    for (size_t i = 0; i < TRANSFORMS_COUNT; i++)
        if (TRANSFORMS[i]->check_request(request_string))
            return TRANSFORMS[i];
    return NULL;
}
