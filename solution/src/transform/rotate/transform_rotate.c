#include "image/imageutils.h"
#include "transform/rotate/transform_rotate.h"
#include "utils.h"
#include <errno.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

/**
 * Try to parse whole string as signed integer
 * @param string    input string
 * @param dst       pointer to output long variable
 * @return 0 if valid, 1 else
 */
int parse_angle(const char *string, long *dst) {
    errno = 0;
    char *end;
    *dst = strtol(string, &end, 10);
    if (errno != 0 || strlen(end) > 0)
        return 1;
    return 0;
}

/**
 * Predicate for choosing this transform by input string
 * Check if command string contains single integer from set of {0, ±90, ±180, ±270}
 *
 * @param command input commandline string
 * @return 1 if this transform can handle params request
 */
int is_rotate(const char *command) {
    long angle;
    int test = parse_angle(command, &angle);
    return test == 0 &&
           angle % 90 == 0 &&
           labs(angle / 90) <= 3;
}

struct image *rotate_apply(const struct image *src, enum ROTATE_DIR dir) {
    if (src == NULL)
        return NULL;

    uint64_t dstHeight = src->height;
    uint64_t dstWidth = src->width;

    switch (dir) {
        case ROTATE_NONE:
            return image_copy(src);
        case ROTATE_90_CLOCKWISE:
        case ROTATE_90_COUNTERCLOCKWISE:
            dstHeight = src->width;
            dstWidth = src->height;
            break;
        default:
            break;
    }

    struct image *dst = image_create(dstWidth, dstHeight);
    if (dst == NULL)
        return NULL;

    for (size_t row = 0; row < src->height; row++) {
        for (size_t col = 0; col < src->width; col++) {
            uint64_t dstRow, dstCol;

            switch (dir) {
                case ROTATE_90_CLOCKWISE:
                    dstRow = src->width - col - 1;
                    dstCol = row;
                    break;
                case ROTATE_90_COUNTERCLOCKWISE:
                    dstRow = col;
                    dstCol = src->height - row - 1;
                    break;
                case ROTATE_HORIZONTAL_REFLECT:
                    dstRow = src->height - row - 1;
                    dstCol = src->width - col - 1;
                    break;
                default:
                    return NULL;
            }

            image_pixel_set(
                    dst,
                    dstRow, dstCol,
                    image_pixel_get(src, row, col)
            );
        }
    }
    return dst;
}


/**
 * Image transform supporting rotating images by angle divisible by 90 degrees
 * @param src input image
 * @param params string that should have integer representing integer angle in degrees from {0, ±90, ±180, ±270}
 * @return transformed image or NULL if transform failed
 */
struct image *rotate(const struct image *src, const char *params) {
    if (src == NULL)
        return NULL;

    long angle;
    if (parse_angle(params, &angle) != 0) {
        printf_err("rotate transform: Parameter string should be and integer angle number");
        return NULL;
    }

    // here for optimization we won't implement real rotation on any degree, just ±90 and 180 in one single full iteration
    angle = (angle + 360) % 360;    // normalize to [0, 360) then to [0, 4)
    angle /= 90;

    return rotate_apply(src, angle);
}

const struct transform_def ROTATE_TRANSFORM = {
        .apply          = rotate,
        .check_request  = is_rotate,
        .name           = "Rotate",
        .info_text      = "Rotate image by 0, ±90, ±180, ±270 degrees. transform-args: <degree>"
};
